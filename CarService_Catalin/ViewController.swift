//
//  ViewController.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var dictionary = [(String, NSDictionary)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        firebaseTest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func firebaseTest() {
//        let clientInstance = ClientDetailsCRUD()
//        let carInstance = CarDetailsCRUD()
//        let serviceInstance = ServiceDetailsCRUD()
//
//        let form = DateFormatter()
//        form.dateFormat = "dd.MM.yyyy"

//        // create entity
//        let client = clientInstance.createClient(clientName: "abcdefg", clientPhone: 123456)
//        let car = carInstance.createCar(ownerId: (client?.clientId)!, carType: "aaa")
//        let service = serviceInstance.createServiceTask(clientId: (client?.clientId!)!, carId: (car?.carId)!,
//                                                             date: form.string(from: Date()),
//                                                             carProblem: "directie", problemSolved: nil)
        
//        // read entity
//        clientInstance.readClientById(id: "-LNQ7YGHaTVldbKBbjJf", completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let client = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "error", message: "Client couldn' be readed!")
//                    return
//                }
//
//                print(client.clientId)
//            }
//        })
//
//        carInstance.readCarById(id: "-LNQ7YP9Ti_LDF5HQkJf", completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let car = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "error", message: "Car couldn' be readed!")
//                    return
//                }
//
//                print(car.carId)
//            }
//        })
//
//        serviceInstance.readServiceById(id: "-LNQ7Ya8CkYuRlBvYK0O", completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let service = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "error", message: "Service couldn' be readed!")
//                    return
//                }
//
//                print(service.serviceId)
//            }
//        })

//        // update entity
//        let newClient = ClientDetails(name: "qwuryq wrqmbw", phone: 13513413412314)
//        clientInstance.updateClientById(id: "-LNQ7YGHaTVldbKBbjJf", updateClient: newClient, completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let client = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Client couldn't be updated!")
//                    return
//                }
//
//                print(client.name)
//            }
//        })
//
//        let newCar = CarDetails(ownerId: "-LNQ7YGHaTVldbKBbjJf", carType: "vw")
//        carInstance.updateCarById(id: "-LNQ7YP9Ti_LDF5HQkJf", updateCar: newCar, completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let car = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Car couldn't be updated!")
//                    return
//                }
//
//                print(car.carType)
//            }
//        })
//
//        let newService = ServiceDetails(clientId: "-LNQ7YGHaTVldbKBbjJf", carId: "-LNQ7YP9Ti_LDF5HQkJf",
//                                        date: form.string(from: Date(timeIntervalSinceNow: 5)),
//                                        carProblem: "abs", problemSolved: "directie")
//        serviceInstance.updateServiceById(id: "-LNQ7Ya8CkYuRlBvYK0O", updateService: newService, completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let service = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "Error", message: "Service couldn't be updated!")
//                    return
//                }
//
//                print(service.getCarProblem())
//            }
//        })

//        // delete entity
//        clientInstance.deleteClientById(id: "-LNQ7YGHaTVldbKBbjJf", key: ClientValues.name, completionHandler: { response, error in
//            guard let client = response else {
//                PopupAlert.popup.createAlert(controller: self, title: "Error", message: "This data couldn't be removed!")
//                return
//            }
//
//            print(client)
//        })
//
//        carInstance.deleteCarById(id: "-LNQ7YP9Ti_LDF5HQkJf", key: nil, completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let car = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "Error", message: "This data couldn't be removed!")
//                    return
//                }
//
//                print(car)
//            }
//        })
//
//        serviceInstance.deleteServiceById(id: "-LNQ7Ya8CkYuRlBvYK0O", key: ServiceValues.date, completionHandler: { response, error in
//            DispatchQueue.main.async {
//                guard let service = response else {
//                    PopupAlert.popup.createAlert(controller: self, title: "Error", message: "This data couldn't be removed!")
//                    return
//                }
//
//                print(service)
//            }
//        })
    }

}

