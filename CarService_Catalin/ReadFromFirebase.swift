//
//  ReadFromFirebase.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit
import Firebase

class ReadFromFirebase {
    var ref = Database.database().reference()
    var dictionary: NSDictionary!
    var uniqeKey: String!
    
    func readFromFirebase(from child: String, completionHandler: @escaping ((String, NSDictionary)?, Error?) -> Void) {
        ref.child(child).observe(.value, with: { snapshot in
            if let datas = snapshot.children.allObjects as? [DataSnapshot] {
                for every in datas {
                    if let dictionary = every.value as? NSDictionary {
                        completionHandler((every.key, dictionary), nil)
                    } else {
                        completionHandler(nil, NSError(domain:"", code:401, userInfo:
                                        [NSLocalizedDescriptionKey: "Missing key or value!"]))
                    }
                }
            }
        })
    }
    
    func readById(from child: String, id: String, completionHandler: @escaping (Any?, Error?) -> Void) {
        
        ref.child(child).child(id).observe(.value, with: { snapshot in
            if let model = snapshot.value {
                completionHandler(model, nil)
            } else {
                completionHandler(nil, NSError(domain:"", code:401, userInfo:
                    [NSLocalizedDescriptionKey: "Data not found!"]))
            }
        })
        
    }
}
