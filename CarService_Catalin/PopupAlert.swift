//
//  PopupAlert.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 25/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class PopupAlert {
    static let popup = PopupAlert()
    
    // alert pop-up window
    func createAlert(controller: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
}
