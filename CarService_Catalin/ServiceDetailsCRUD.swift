//
//  ServiceDetailsCrud.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 26/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class ServiceDetailsCRUD {
    let child = ChildType.service
    
    func createServiceTask(clientId: String, carId: String, date: String,
                           carProblem: String, problemSolved: String?) -> ServiceDetails? {
        
        let serviceTask = ServiceDetails.init(clientId: clientId, carId: carId, date: date,
                                              carProblem: carProblem, problemSolved: problemSolved)
        
        return CreateFirebaseEntity().createEntity(entity: serviceTask) as? ServiceDetails
    }
    
    func readServiceById(id: String, completionHandler: @escaping (ServiceDetails?, Error?) -> Void) {
        ReadFromFirebase().readById(from: child, id: id, completionHandler: { response, error in
            guard var serviceDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            serviceDictionary["serviceId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: serviceDictionary, options: [])
                
                let service = try JSONDecoder().decode(ServiceDetails.self, from: jsonData)
                
                completionHandler(service, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    func updateServiceById(id: String, updateService: ServiceDetails,
                       completionHandler: @escaping (ServiceDetails?, Error?) -> Void) {
        guard let jsonData = try? JSONEncoder().encode(updateService) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Couldn't convert data!"]))
            return
        }
        
        let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)

        UpdateFirebaseEntity().updateEntity(child: child, id: id, updateObject: dictionary as Any,
                                            completionHandler: { response, error in
            guard var serviceDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            serviceDictionary["serviceId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: serviceDictionary, options: [])
                
                let service = try JSONDecoder().decode(ServiceDetails.self, from: jsonData)
                
                completionHandler(service, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    func deleteServiceById(id: String, key: String?, completionHandler: @escaping (ServiceDetails?, Error?) -> Void) {
        DeleteFirebaseEntity().deleteEntity(child: child, id: id, key: key, completionHandler: { response, error in
            guard var serviceDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            serviceDictionary["serviceId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: serviceDictionary, options: [])
                
                let service = try JSONDecoder().decode(ServiceDetails.self, from: jsonData)
                
                completionHandler(service, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
}
