//
//  UpdateFirebaseEntity.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit
import Firebase

class UpdateFirebaseEntity {
    var ref = Database.database().reference()

    func updateEntity(child: String, id: String, updateObject: Any, completionHandler: @escaping (Any?, Error?) -> Void) {
        
        if let dictionary = updateObject as? [String:Any] {
            dictionary.forEach { (key: String, value: Any) in
                ref.child(child).child(id).updateChildValues([key:value])
                completionHandler(dictionary, nil)
            }
        } else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Couldn't update data!"]))
        }
       
    }
}
