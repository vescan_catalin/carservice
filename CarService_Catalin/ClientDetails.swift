//
//  ClientDetails.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class ClientDetails: Codable {
    var clientId: String?
    private(set) var name: String
    private(set) var phone: Int
    
    init(name: String, phone: Int) {
        self.name = name
        self.phone = phone
    }
    
}
