//
//  FirebaseEnum.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 25/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import Foundation

enum ChildType {
    static let client = "client"
    static let car = "car"
    static let service = "service"
}

enum ClientValues {
    static let name = "name"
    static let phone = "phone"
}

enum CarValues {
    static let ownerId = "ownerId"
    static let carType = "carType"
}

enum ServiceValues {
    static let clientId = "clientId"
    static let carId = "carId"
    static let date = "date"
    static let carProblem = "carProblem"
    static let problemSolved = "problemSolved"
}
