//
//  CarDetailsCRUD.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 26/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class CarDetailsCRUD {
    let child = ChildType.car
    
    func createCar(ownerId: String, carType: String) -> CarDetails? {
        let car = CarDetails.init(ownerId: ownerId, carType: carType)
        
        return CreateFirebaseEntity().createEntity(entity: car) as? CarDetails
    }
    
    func readCarById(id: String, completionHandler: @escaping (CarDetails?, Error?) -> Void) {
        ReadFromFirebase().readById(from: child, id: id, completionHandler: { response, error in
            guard var carDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            carDictionary["carId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: carDictionary, options: [])
                
                let car = try JSONDecoder().decode(CarDetails.self, from: jsonData)
                
                completionHandler(car, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    func updateCarById(id: String, updateCar: CarDetails,
                       completionHandler: @escaping (CarDetails?, Error?) -> Void) {
        guard let jsonData = try? JSONEncoder().encode(updateCar) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Couldn't convert data!"]))
            return
        }
        
        let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)

        UpdateFirebaseEntity().updateEntity(child: child, id: id, updateObject: dictionary as Any,
                                            completionHandler: { response, error in
            guard var carDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            carDictionary["carId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: carDictionary, options: [])
                
                let car = try JSONDecoder().decode(CarDetails.self, from: jsonData)
                
                completionHandler(car, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    func deleteCarById(id: String, key: String?, completionHandler: @escaping (CarDetails?, Error?) -> Void) {
        DeleteFirebaseEntity().deleteEntity(child: child, id: id, key: key, completionHandler: { response, error in
            guard var carDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            carDictionary["carId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: carDictionary, options: [])
                
                let car = try JSONDecoder().decode(CarDetails.self, from: jsonData)
                
                completionHandler(car, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
}
