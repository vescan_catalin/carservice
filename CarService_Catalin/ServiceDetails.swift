//
//  ServiceDetails.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class ServiceDetails: Codable {
    var serviceId: String?
    private var clientId: String?
    private var carId: String?
    private var date: String?
    private var carProblem: String?
    private var problemSolved: String?
    
    init(clientId: String, carId: String, date: String, carProblem: String, problemSolved: String?) {
        self.clientId = clientId
        self.carId = carId
        self.date = date
        self.carProblem = carProblem
        self.problemSolved = problemSolved
    }
    
    func getClientId() -> String {
        if let clientId = self.clientId {
            return clientId
        }
        
        return ""
    }
    
    func getCarId() -> String {
        if let carId = self.carId {
            return carId
        }
        
        return ""
    }
    
    func getDate() -> String {
        if let date = self.date {
            return date
        }
        
        return ""
    }
    
    func getCarProblem() -> String {
        if let carProblem = self.carProblem {
            return carProblem
        }
        
        return ""
    }
    
    func getProblemSolved() -> String {
        if let problemSolved = self.problemSolved {
            return problemSolved
        }
        
        return ""
    }
    
}
