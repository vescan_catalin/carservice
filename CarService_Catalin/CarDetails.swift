//
//  CarDetails.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class CarDetails: Codable {
    var carId: String?
    private(set) var ownerId: String?
    private(set) var carType: String?
    
    init(ownerId: String, carType: String) {
        self.ownerId = ownerId
        self.carType = carType
    }
    
}
