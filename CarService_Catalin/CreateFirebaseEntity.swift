//
//  CreateFirebaseEntity.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit
import Firebase

class CreateFirebaseEntity {
    var ref = Database.database().reference()
    
    func createEntity(entity: Any) -> Any? {
        switch entity {
        case is ClientDetails: do {
            let newRef = self.ref.child("client").childByAutoId()
            newRef.child("name").setValue((entity as? ClientDetails)?.name)
            newRef.child("phone").setValue((entity as? ClientDetails)?.phone)
            
            if let client = entity as? ClientDetails {
                client.clientId = newRef.key
                return client
            }
            
            return nil
            }
        case is CarDetails: do {
            let newRef = self.ref.child("car").childByAutoId()
            newRef.child("ownerId").setValue((entity as? CarDetails)?.ownerId)
            newRef.child("carType").setValue((entity as? CarDetails)?.carType)
            
            if let car = entity as? CarDetails {
                car.carId = newRef.key
                return car
            }
            
            return nil
            }
        case is ServiceDetails: do {
            let newRef = self.ref.child("service").childByAutoId()
            newRef.child("clientId").setValue((entity as? ServiceDetails)?.getClientId())
            newRef.child("carId").setValue((entity as? ServiceDetails)?.getCarId())
            newRef.child("date").setValue((entity as? ServiceDetails)?.getDate())
            newRef.child("carProblem").setValue((entity as? ServiceDetails)?.getCarProblem())
            newRef.child("problemSolved").setValue((entity as? ServiceDetails)?.getProblemSolved())
            
            if let service = entity as? ServiceDetails {
                service.serviceId = newRef.key
                return service
            }
            
            return nil
            }
        default: print("wrong entity")
        }
        
        return nil
    }
    
}
