//
//  ClientDetailsCRUD.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 26/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit

class ClientDetailsCRUD {
    let child = ChildType.client
    
    func createClient(clientName: String, clientPhone: Int) -> ClientDetails? {
        let client = ClientDetails.init(name: clientName, phone: clientPhone)
        
        return CreateFirebaseEntity().createEntity(entity: client) as? ClientDetails
    }
    
    func readClientById(id: String, completionHandler: @escaping (ClientDetails?, Error?) -> Void) {
        ReadFromFirebase().readById(from: child, id: id, completionHandler: { response, error in
            guard var clientDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            clientDictionary["clientId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: clientDictionary, options: [])
                
                let client = try JSONDecoder().decode(ClientDetails.self, from: jsonData)
                
                completionHandler(client, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    func updateClientById(id: String, updateClient: ClientDetails,
                          completionHandler: @escaping (ClientDetails?, Error?) -> Void) {
        guard let jsonData = try? JSONEncoder().encode(updateClient) else {
            completionHandler(nil, NSError(domain:"", code:401, userInfo:
                [NSLocalizedDescriptionKey: "Couldn't convert data!"]))
            return
        }
        
        let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)

        UpdateFirebaseEntity().updateEntity(child: child, id: id, updateObject: dictionary as Any,
                                            completionHandler: { response, error in
            guard var clientDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }

            clientDictionary["clientId"] = id

            do {
                let jsonData = try JSONSerialization.data(withJSONObject: clientDictionary, options: [])

                let client = try JSONDecoder().decode(ClientDetails.self, from: jsonData)

                completionHandler(client, nil)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    func deleteClientById(id: String, key: String?, completionHandler: @escaping (ClientDetails?, Error?) -> Void) {
        DeleteFirebaseEntity().deleteEntity(child: child, id: id, key: key, completionHandler: { response, error in
            guard var clientDictionary = response as? [String:Any] else {
                completionHandler(nil, error)
                return
            }
            
            clientDictionary["clientId"] = id
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: clientDictionary, options: [])
                
                let client = try JSONDecoder().decode(ClientDetails.self, from: jsonData)
                
                completionHandler(client, nil)
            } catch {
                print(error.localizedDescription)
            }
            
        })
    }
    
}
