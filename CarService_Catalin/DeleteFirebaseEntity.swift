//
//  DeleteFirebaseEntity.swift
//  CarService_Catalin
//
//  Created by Intern Device1 on 24/09/2018.
//  Copyright © 2018 Vescan Catalin. All rights reserved.
//

import UIKit
import Firebase

class DeleteFirebaseEntity {
    var ref = Database.database().reference()

    func deleteEntity(child: String, id: String, key: String?, completionHandler: @escaping (Any?, Error?) -> Void) {
        if let key = key {
            ref.child(child).child(id).observe(.value, with: { snapshot in
                if snapshot.hasChild(key) {
                    self.ref.child(child).child(id).child(key).removeValue()
                    if let entity = snapshot.value {
                        completionHandler(entity, nil)
                    } else {
                        completionHandler(nil, NSError(domain:"", code:401, userInfo:
                            [NSLocalizedDescriptionKey: "Invalid data!"]))
                    }
                }
            })
        } else {
            ref.child(child).observe(.value, with: { snapshot in
                if snapshot.hasChild(id) {
                    self.ref.child(child).child(id).removeValue()
                }
            })
        }
        
    }
}
